namespace Code_First_Demo.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesNametoString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MyFirstTables", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MyFirstTables", "Name", c => c.Int(nullable: false));
        }
    }
}
